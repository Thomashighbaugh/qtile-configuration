##### COLORS #####
colors = [["#2d3f3d", "#1e2026"],  # panel background
          ["#2d2f3d", "#1e2026"],  # background for current screen tab
          ["#edeeff", "#edeeff"],  # font color for group names
          ["#00caff", "#00caff"],  # border line color for current tab
          ["#00eaff", "#00caff"],  # border line color for other tab and odd widgets
          ["#ff9cff", "#f850ac"],  # color for the even widgets
          ["#edeeff", "#edeeff"]]  # window name
